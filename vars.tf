variable "sleepy_time" {
  description = "sleep time"
  default     = 548
}

variable "sl_user_name" {
  description = "sl user name"
  default     = "vishwa"
}

variable "cluster_name" {
  description = "Name of the cluster"
  default     = ""
}

variable "datacenter" {
  description = "Datacenter"
  default     = "frankfut"
}

variable "sample_var" {
  description = "A sample_var to pass to the template."
  default     = "sample var"
}